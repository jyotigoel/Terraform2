############# public instance security group ################

resource "aws_security_group" "pub_instance-sg" {
  vpc_id = var.vpc_id
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.pub_SG_ssh_cidr_block
  }

  egress {
    description = "all traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Nginx_pub_sg"
  }
}

############# ossec server instance security group ################

resource "aws_security_group" "pvt_instance-sg" {
  vpc_id = var.vpc_id
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.vpc_cidr_block
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.vpc_cidr_block
  }


  ingress {
    description     = "HTTP"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [var.alb_sg_id]
  }

  egress {
    description = "all traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Nginx_pvt_sg"
  }
}



resource "aws_instance" "pub_ec2_instance" {
  count           = length(var.pub_instances_name)
  subnet_id       = var.pub_subnet_id[count.index]
  ami             = var.instance_ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.pub_instance-sg.id]
  ebs_optimized   = false
  # ecs_associate_public_ip_address = true

  tags = merge(
    var.additional_tags,
    {
      Name = var.pub_instances_name[count.index]
    },
  )
}

###################### server instances ###########

resource "aws_instance" "pvt_ec2_instance" {
  count           = length(var.pvt_instances_name)
  subnet_id       = var.pvt_subnet_id[count.index]
  ami             = var.instance_ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.pvt_instance-sg.id]
  ebs_optimized   = false
  tags = merge(
    var.additional_tags,
    {
      Name = var.pvt_instances_name[count.index]
    },
  )
}


################# agent security group #####################

resource "aws_security_group" "agent_instances-sg" {
  vpc_id = var.vpc_id
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.vpc_cidr_block
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.vpc_cidr_block
  }



  egress {
    description = "all traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "agent_sg"
  }
}

resource "aws_instance" "agent_ec2_instance" {
  count           = length(var.agent_instances_name)
  subnet_id       = var.pvt_subnet_id[count.index]
  ami             = var.instance_ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.agent_instances-sg.id]
  ebs_optimized   = false
  tags = merge(
    var.additional_tags,
    {
      Name = var.agent_instances_name[count.index]
    },
  )
}
