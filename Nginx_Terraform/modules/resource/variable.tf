variable "vpc_id" {
  type = string
}

variable "pub_SG_ssh_cidr_block" {
  type = list(any)
}

variable "pub_instances_name" {
  type = list(string)
}

variable "pub_subnet_id" {
  type = list(any)
}

variable "instance_ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "additional_tags" {
  default = {}
  type    = map(any)
}

variable "vpc_cidr_block" {
  type = list(string)
}

variable "pvt_instances_name" {
  type = list(any)
}

variable "pvt_subnet_id" {
  type = list(any)
}

variable "alb_sg_id" {
  type = string
}

variable "agent_instances_name" {
  type = list(any)
}