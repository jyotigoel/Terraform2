########### target group ########

resource "aws_alb_target_group" "nginx_server_tg" {
  name     = var.target_group_name
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

########### target_group_attachment

resource "aws_alb_target_group_attachment" "nginx_server_instance_attachment" {
  target_group_arn = aws_alb_target_group.nginx_server_tg.arn
  target_id        = var.target_instance_id
  port             = 80
}


############ Application load balancer ########

resource "aws_security_group" "alb_sg" {
  vpc_id = var.vpc_id
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.alb_sg_cidr
  }

  egress {
    description = "all traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Nginx_alb_sg"
  }
}


resource "aws_alb" "my_alb" {  
  name            = var.alb_name
  subnets         = var.alb_subnets
  security_groups = [aws_security_group.alb_sg.id]
  internal        = false
#   idle_timeout    = "${var.idle_timeout}"   
  tags = {    
    Name    = var.alb_name    
  }   

}

resource "aws_alb_listener" "nginx_alb_listener" {
  load_balancer_arn = aws_alb.my_alb.arn
  port = "80"
  protocol = "HTTP"
  
  default_action {
    target_group_arn = aws_alb_target_group.nginx_server_tg.arn
    type             = "forward"
  }
}
