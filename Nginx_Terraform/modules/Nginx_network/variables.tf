variable "vpc_cidr_block" {
  type        = string
  description = "vpc CIDR block"
}

variable "dns_hostnames" {
  type        = bool
  description = "enable or disable dns hostnames"
}
variable "vpc_tenancy" {
  type        = string
  description = "tenancy type"
}

variable "additional_tags" {
  default = {}
  type    = map(any)
}

variable "igw_name" {
  type = string
}

variable "pub_subnet_Name" {
  type = list(any)
}

variable "pub_subnet_CIDR" {
  type = list(any)
}

variable "pvt_subnet_Name" {
  type = list(any)
}

variable "pvt_subnet_CIDR" {
  type = list(any)
}

variable "public_available_zone" {
  type = list(any)
}

variable "private_available_zone" {
  type = list(any)
}

variable "nat_gw_name" {
  type = string
}

variable "pub_route_table_name" {
  type = string
}

variable "pvt_route_table_name" {
  type = string
}
