output "vpc_id_output" {
  value = aws_vpc.nginx_server_vpc.id
}

output "vpc_cidr_output" {
  value = aws_vpc.nginx_server_vpc.cidr_block
}
output "pub_subnet_id_output" {
  value = aws_subnet.nginx_pub_subnets.*.id
}

output "pvt_subnet_id_output" {
  value = aws_subnet.nginx_pvt_subnets.*.id
}

