region  = "us-east-1"

########### VPC #############
vpc_cidr             = "10.0.0.0/20"
vpc_Name             = "Nginx_VPC_1"
enable_dns_hostname  = "true"
vpc_instance_tenancy = "default"
additional_tags      = { tool = "nginx", Team = "Ninja", }

#########   igw  #######

igw_name = "Nginx_igw_1"

########  subnets  #######

pub_sn_name = ["Nginx_pub_sn1", "Nginx_pub_sn2"]
pub_sn_cidr = ["10.0.0.0/22", "10.0.4.0/22"]

pvt_sn_name = ["Nginx_pvt_sn1", "Nginx_pvt_sn2", "Nginx_pvt_sn3", "Nginx_pvt_sn4"]
pvt_sn_cidr = ["10.0.8.0/23", "10.0.10.0/23", "10.0.12.0/23", "10.0.14.0/23"]

######## pub_subnet_availability_zones ###########
public_sn_available_zone = ["us-east-1a", "us-east-1b"]

######## pub_subnet_availability_zones ###########
private_sn_available_zone = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1a"]

############ NAT gateway #####################
NAT_GW_Name = "Nginx_NAT_GW"

############ Route_tables ####################
public_RT_Name  = "Nginx_public_RT"
private_RT_Name = "Nginx_private_RT"

########### Security group ###################
ssh_security_group_cidr = ["172.31.0.0/16"]
########### public instance ################
pub_instances_name = ["Nginx_vpn_server", ]
instance_ami       = "ami-094977b28e7b98edc"
instance_type      = "t2.micro"
key_name           = "Key-pair"

############ private instances #############

pvt_instances_name = ["Nginx_host1"]

############ NGINX agent instance ##########
agent_instances_name = ["Nginx_host2"]

############ load balancer ################

load_balacer_name = "Nginx-lb"

alb_sg_cidr = ["183.83.209.185/32"]

target_group_name = "nginx-server-tg"
