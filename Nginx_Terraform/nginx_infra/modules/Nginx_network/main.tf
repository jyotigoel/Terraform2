###################### VPC ########################

resource "aws_vpc" "nginx_server_vpc" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = var.vpc_tenancy
  enable_dns_hostnames = var.dns_hostnames
  tags = merge(
    var.additional_tags,
    {
      Name = var.vpc_name
    },
  )
}
#################### VPC Peering ########################
# Owner details
data "aws_vpc" "vpc" {
  state = "available"
  filter {
    name = "tag:Name"
    values = ["Default_VPC"]
  }
}
# Details of existing Route table
data "aws_route_table" "routetable" {
  filter {
    name = "tag:Name"
    values = ["Default_RT"]
  }
}
# Owner details
data "aws_caller_identity" "owner" {
}
data "aws_security_group" "securitygroup" {
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    "Name" = "my-sg"
  }
}

resource "aws_vpc_peering_connection" "vpc" {
  peer_owner_id = data.aws_caller_identity.owner.account_id
  peer_vpc_id   = data.aws_vpc.vpc.id
  vpc_id        = aws_vpc.nginx_server_vpc.id
  auto_accept   = true
}
resource "aws_route" "peering_1" {
  route_table_id            = aws_route_table.nginx_pvt_rt.id
  destination_cidr_block    = data.aws_vpc.vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc.id
}
resource "aws_route" "peering_2" {
  route_table_id            = data.aws_route_table.routetable.id
  destination_cidr_block    = var.vpc_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc.id
}

resource "aws_route" "peering_3" {
  route_table_id            = aws_route_table.nginx_pub_rt.id
  destination_cidr_block    = data.aws_vpc.vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc.id
}

#################### internet gateway ###################

resource "aws_internet_gateway" "nginx_igw" {
  vpc_id = aws_vpc.nginx_server_vpc.id
  tags = merge(
    var.additional_tags,
    {
      Name = var.igw_name
    },
  )
}

############################ subnets ################

resource "aws_subnet" "nginx_pub_subnets" {
  count                   = length(var.pub_subnet_Name)
  vpc_id                  = aws_vpc.nginx_server_vpc.id
  cidr_block              = var.pub_subnet_CIDR[count.index]
  availability_zone       = var.public_available_zone[count.index]
  map_public_ip_on_launch = true 
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.pub_subnet_Name[count.index]}"
    },
  )
}


resource "aws_subnet" "nginx_pvt_subnets" {
  count                   = length(var.pvt_subnet_Name)
  vpc_id                  = aws_vpc.nginx_server_vpc.id
  cidr_block              = var.pvt_subnet_CIDR[count.index]
  availability_zone       = var.private_available_zone[count.index]
  map_public_ip_on_launch = false
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.pvt_subnet_Name[count.index]}"
    },
  )
}

############################# NAT gateway  ####################

resource "aws_eip" "nginx_eip" {
  count = 1
}

resource "aws_nat_gateway" "nginx_NAT_GW" {
  allocation_id = aws_eip.nginx_eip[0].id
  subnet_id     = aws_subnet.nginx_pub_subnets[0].id

  tags = merge(
    var.additional_tags,
    {
      Name = var.nat_gw_name
    },
  )
}

############################# Public Route tables ####################

resource "aws_route_table" "nginx_pub_rt" {
  vpc_id = aws_vpc.nginx_server_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nginx_igw.id

  }

  tags = merge(
    var.additional_tags, {
      Name = var.pub_route_table_name
    },
  )
}

resource "aws_route_table_association" "associate_public_subnets" {
  count          = length(var.pub_subnet_Name)
  subnet_id      = element(aws_subnet.nginx_pub_subnets.*.id, count.index)
  route_table_id = aws_route_table.nginx_pub_rt.id
}

############################ Private Route Table #######################

resource "aws_route_table" "nginx_pvt_rt" {
  vpc_id = aws_vpc.nginx_server_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nginx_NAT_GW.id

  }

  tags = merge(
    var.additional_tags, {
      Name = var.pvt_route_table_name
    },
  )
}

resource "aws_route_table_association" "associate_private_subnets" {
  count          = length(var.pvt_subnet_Name)
  subnet_id      = element(aws_subnet.nginx_pvt_subnets.*.id, count.index)
  route_table_id = aws_route_table.nginx_pvt_rt.id
}
